/*
Navicat MySQL Data Transfer

Source Server         : msk
Source Server Version : 80022
Source Host           : localhost:3306
Source Database       : hospital

Target Server Type    : MYSQL
Target Server Version : 80022
File Encoding         : 65001

Date: 2021-09-02 18:08:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `charge`
-- ----------------------------
DROP TABLE IF EXISTS `charge`;
CREATE TABLE `charge` (
  `id` int NOT NULL AUTO_INCREMENT,
  `chargeName` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `chargeMoney` double NOT NULL DEFAULT '0',
  `chargeDate` date DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  `isDel` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- ----------------------------
-- Records of charge
-- ----------------------------
INSERT INTO `charge` VALUES ('1', 'X光', '300', '2021-08-04', '2021-08-25', '1');
INSERT INTO `charge` VALUES ('2', 'B超', '500', '2021-08-11', '2021-08-18', '0');
INSERT INTO `charge` VALUES ('3', '彩超', '300', '2021-08-19', '2021-08-03', '0');
INSERT INTO `charge` VALUES ('4', '产检', '100', '2021-08-04', '2021-08-11', '0');
INSERT INTO `charge` VALUES ('5', '血常规', '200', '2021-08-12', '2021-08-25', '0');
INSERT INTO `charge` VALUES ('6', '尿检', '100', '2021-08-26', '2021-08-18', '0');

-- ----------------------------
-- Table structure for `chargehospital`
-- ----------------------------
DROP TABLE IF EXISTS `chargehospital`;
CREATE TABLE `chargehospital` (
  `id` int NOT NULL AUTO_INCREMENT,
  `chargeId` int NOT NULL,
  `hospitalId` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `g` (`chargeId`),
  KEY `h` (`hospitalId`),
  CONSTRAINT `g` FOREIGN KEY (`chargeId`) REFERENCES `charge` (`id`),
  CONSTRAINT `h` FOREIGN KEY (`hospitalId`) REFERENCES `hospital` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- ----------------------------
-- Records of chargehospital
-- ----------------------------
INSERT INTO `chargehospital` VALUES ('1', '1', '1');
INSERT INTO `chargehospital` VALUES ('2', '2', '1');
INSERT INTO `chargehospital` VALUES ('3', '3', '1');
INSERT INTO `chargehospital` VALUES ('4', '4', '1');
INSERT INTO `chargehospital` VALUES ('5', '5', '1');
INSERT INTO `chargehospital` VALUES ('6', '6', '1');

-- ----------------------------
-- Table structure for `doctor`
-- ----------------------------
DROP TABLE IF EXISTS `doctor`;
CREATE TABLE `doctor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `doctorName` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `idNumber` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `telPhone` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `sex` int NOT NULL DEFAULT '1',
  `birthday` date DEFAULT NULL,
  `age` int NOT NULL DEFAULT '18',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `department` int NOT NULL DEFAULT '0',
  `eduction` int NOT NULL DEFAULT '0',
  `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `inTime` date DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `bb` (`idNumber`),
  UNIQUE KEY `cc` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- ----------------------------
-- Records of doctor
-- ----------------------------
INSERT INTO `doctor` VALUES ('1', '张三丰', '141181199306250098', '18335265624', '03587210096', '1', '2021-08-18', '18', '123@qq.com', '0', '0', null, null, '0');

-- ----------------------------
-- Table structure for `drug`
-- ----------------------------
DROP TABLE IF EXISTS `drug`;
CREATE TABLE `drug` (
  `id` int NOT NULL AUTO_INCREMENT,
  `img` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `inPrice` double NOT NULL DEFAULT '0',
  `outPrice` double NOT NULL DEFAULT '0',
  `drugName` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `drugType` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `quality` int NOT NULL DEFAULT '0',
  `detailDescription` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `factory` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `instruction` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `note` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `totalNumber` int NOT NULL DEFAULT '0',
  `sentNumber` int NOT NULL DEFAULT '0',
  `resetNumber` int NOT NULL DEFAULT '0',
  `status` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- ----------------------------
-- Records of drug
-- ----------------------------
INSERT INTO `drug` VALUES ('1', null, '0', '0', '阿莫西林', '非处方药', null, '0', null, null, null, null, '0', '0', '0', '0');
INSERT INTO `drug` VALUES ('4', null, '50', '100', '无比滴', '非处方', '止痛', '12', '用于缓解轻至中度疼痛', '通化茂祥', '一日三次', '孕妇禁用', '50', '10', '40', '0');
INSERT INTO `drug` VALUES ('5', null, '20', '30', '奥硝胶囊', '西药', '牙痛药', '12', '用于缓解轻至中度疼痛', '通化茂祥', '一日两次', '孕妇禁用', '50', '10', '40', '0');
INSERT INTO `drug` VALUES ('6', null, '10', '20', '云南白药', '非处方', '外敷药', '12', '用于缓解轻至中度疼痛', '通化茂祥', '一日两次', '孕妇禁用', '50', '10', '40', '0');
INSERT INTO `drug` VALUES ('7', null, '5', '10', '布洛芬', '非处方', '止痛', '12', '用于缓解轻至中度疼痛', '通化茂祥', '一日两次', '孕妇禁用', '50', '10', '40', '0');
INSERT INTO `drug` VALUES ('8', null, '15', '20', '止咳糖浆', '非处方', '止咳药', '12', '用于缓解轻至中度疼痛', '通化茂祥', '一日一次', '孕妇禁用', '50', '10', '40', '0');

-- ----------------------------
-- Table structure for `drughospital`
-- ----------------------------
DROP TABLE IF EXISTS `drughospital`;
CREATE TABLE `drughospital` (
  `id` int NOT NULL AUTO_INCREMENT,
  `drugId` int NOT NULL,
  `hospitalId` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `e` (`drugId`),
  KEY `f` (`hospitalId`),
  CONSTRAINT `e` FOREIGN KEY (`drugId`) REFERENCES `drug` (`id`),
  CONSTRAINT `f` FOREIGN KEY (`hospitalId`) REFERENCES `hospital` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- ----------------------------
-- Records of drughospital
-- ----------------------------
INSERT INTO `drughospital` VALUES ('1', '1', '1');

-- ----------------------------
-- Table structure for `hospital`
-- ----------------------------
DROP TABLE IF EXISTS `hospital`;
CREATE TABLE `hospital` (
  `id` int NOT NULL AUTO_INCREMENT,
  `bedNumber` int NOT NULL DEFAULT '0',
  `deposit` double NOT NULL DEFAULT '0',
  `introduction` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `endStatus` int NOT NULL DEFAULT '0',
  `infoStatus` int NOT NULL DEFAULT '0',
  `registerId` int NOT NULL,
  `hospitalDate` date NOT NULL,
  `totalCost` int NOT NULL DEFAULT '0',
  `restCost` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i` (`registerId`),
  CONSTRAINT `i` FOREIGN KEY (`registerId`) REFERENCES `register` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- ----------------------------
-- Records of hospital
-- ----------------------------
INSERT INTO `hospital` VALUES ('1', '88', '3000', null, '0', '0', '1', '2021-08-11', '0', '0');
INSERT INTO `hospital` VALUES ('2', '99', '0', null, '0', '0', '2', '2021-08-04', '0', '0');
INSERT INTO `hospital` VALUES ('3', '55', '0', null, '0', '0', '3', '2021-07-30', '0', '0');
INSERT INTO `hospital` VALUES ('4', '56', '0', null, '0', '0', '4', '2021-08-06', '0', '0');
INSERT INTO `hospital` VALUES ('5', '50', '0', null, '0', '0', '5', '2021-08-06', '0', '0');
INSERT INTO `hospital` VALUES ('6', '89', '0', null, '0', '0', '6', '2021-08-14', '0', '0');

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `menuName` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '挂号信息管理', '/registration/index.html', '0');
INSERT INTO `menu` VALUES ('2', '住院办理', '/hospital/index.html', '0');
INSERT INTO `menu` VALUES ('3', '住院结算', '/hospital/account.html', '0');
INSERT INTO `menu` VALUES ('4', '在院发药', '/hospital/dispensing.html', '0');
INSERT INTO `menu` VALUES ('5', '药品管理', '/medicine/index.html', '0');
INSERT INTO `menu` VALUES ('6', '检查收费项目登记', '/hospital/charge2.html', '0');
INSERT INTO `menu` VALUES ('7', '收费项目管理', '/hospital/charge.html', '0');
INSERT INTO `menu` VALUES ('8', '门诊医生管理', '/doctor/index.html', '0');
INSERT INTO `menu` VALUES ('9', '用户管理', '/User/index.html', '0');
INSERT INTO `menu` VALUES ('10', '角色管理', '/Role/index.html', '0');
INSERT INTO `menu` VALUES ('11', '资源管理', '/Resource/index.html', '0');
INSERT INTO `menu` VALUES ('12', '密码管理', '/User/password.html', '0');

-- ----------------------------
-- Table structure for `register`
-- ----------------------------
DROP TABLE IF EXISTS `register`;
CREATE TABLE `register` (
  `id` int NOT NULL AUTO_INCREMENT,
  `registerName` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `idNumber` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `regPrice` double NOT NULL DEFAULT '0',
  `medicalNumber` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `selfPrice` int NOT NULL DEFAULT '0',
  `sex` int NOT NULL DEFAULT '1',
  `age` int NOT NULL DEFAULT '18',
  `profession` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `lookDoctor` int NOT NULL DEFAULT '0',
  `department` int NOT NULL DEFAULT '0',
  `doctorId` int NOT NULL,
  `note` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `dd` (`doctorId`),
  CONSTRAINT `dd` FOREIGN KEY (`doctorId`) REFERENCES `doctor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- ----------------------------
-- Records of register
-- ----------------------------
INSERT INTO `register` VALUES ('1', '张三丰', '141181198603120093', '0', '123456', '18362541234', '0', '1', '18', '狗仔', '0', '0', '1', null, '0');
INSERT INTO `register` VALUES ('2', '李四', null, '0', null, null, '0', '1', '18', null, '0', '0', '1', null, '0');
INSERT INTO `register` VALUES ('3', '张三', null, '0', null, null, '0', '1', '18', null, '0', '0', '1', null, '0');
INSERT INTO `register` VALUES ('4', '王五', null, '0', null, null, '0', '1', '18', null, '0', '0', '1', null, '0');
INSERT INTO `register` VALUES ('5', '马六', null, '0', null, null, '0', '1', '18', null, '0', '0', '1', null, '0');
INSERT INTO `register` VALUES ('6', '二宝', null, '0', null, null, '0', '1', '18', null, '0', '0', '1', null, '0');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `roleName` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `status` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '超级管理员', '0');
INSERT INTO `role` VALUES ('2', '药品管理员', '0');
INSERT INTO `role` VALUES ('3', '医生', '0');
INSERT INTO `role` VALUES ('4', '医院前台', '0');
INSERT INTO `role` VALUES ('5', '住院办理员', '0');
INSERT INTO `role` VALUES ('6', '院长', '0');
INSERT INTO `role` VALUES ('24', '副院长', '0');

-- ----------------------------
-- Table structure for `rolemenu`
-- ----------------------------
DROP TABLE IF EXISTS `rolemenu`;
CREATE TABLE `rolemenu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `roleId` int NOT NULL,
  `menuId` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `b` (`roleId`),
  KEY `c` (`menuId`),
  CONSTRAINT `b` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`),
  CONSTRAINT `c` FOREIGN KEY (`menuId`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- ----------------------------
-- Records of rolemenu
-- ----------------------------
INSERT INTO `rolemenu` VALUES ('1', '1', '1');
INSERT INTO `rolemenu` VALUES ('2', '1', '2');
INSERT INTO `rolemenu` VALUES ('3', '1', '3');
INSERT INTO `rolemenu` VALUES ('4', '1', '4');
INSERT INTO `rolemenu` VALUES ('5', '1', '5');
INSERT INTO `rolemenu` VALUES ('6', '1', '6');
INSERT INTO `rolemenu` VALUES ('7', '1', '7');
INSERT INTO `rolemenu` VALUES ('8', '1', '8');
INSERT INTO `rolemenu` VALUES ('9', '1', '9');
INSERT INTO `rolemenu` VALUES ('10', '1', '10');
INSERT INTO `rolemenu` VALUES ('11', '1', '11');
INSERT INTO `rolemenu` VALUES ('12', '1', '12');

-- ----------------------------
-- Table structure for `sysuser`
-- ----------------------------
DROP TABLE IF EXISTS `sysuser`;
CREATE TABLE `sysuser` (
  `id` int NOT NULL AUTO_INCREMENT,
  `loginName` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `trueName` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `roleId` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `aa` (`loginName`),
  KEY `a` (`roleId`),
  CONSTRAINT `a` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- ----------------------------
-- Records of sysuser
-- ----------------------------
INSERT INTO `sysuser` VALUES ('1', 'admin', 'admin', '张三', '123@qq.com', '0', '1');
INSERT INTO `sysuser` VALUES ('2', 'xixi', 'xixi', '嘻嘻', '123@qq.com', '0', '2');
INSERT INTO `sysuser` VALUES ('3', 'ada', 'ada', '阿达', '35463253@qq.com', '0', '5');
INSERT INTO `sysuser` VALUES ('4', 'heihei', 'heihei', '黑黑', '35463253@qq.com', '0', '3');
INSERT INTO `sysuser` VALUES ('5', 'lele', 'lele', '乐乐', '35463253@qq.com', '0', '5');
INSERT INTO `sysuser` VALUES ('6', 'jiangbei', 'jiangbei', '江北', '35463253@qq.com', '1', '5');
INSERT INTO `sysuser` VALUES ('7', 'yueyue', 'yueyue', '岳岳', '35463253@qq.com', '1', '5');
INSERT INTO `sysuser` VALUES ('8', 'haer', 'haer', '哈尔', '123@qq.com', '1', '2');
