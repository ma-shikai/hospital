
-- 药品表测试数据
INSERT INTO drug (id, img, inPrice, outPrice, drugName, drugType, description, quality, detailDescription, factory, instruction, note, totalNumber, sentNumber, resetNumber, status) VALUES (3, null, 100, 120, '阿莫西林', '非处方', '感冒药', 12, '消炎药', '通化茂祥', '一日两次', '孕妇禁用', 100, 20, 80, 0);
INSERT INTO drug (id, img, inPrice, outPrice, drugName, drugType, description, quality, detailDescription, factory, instruction, note, totalNumber, sentNumber, resetNumber, status) VALUES (4, null, 50, 100, '无比滴', '非处方', '止痛', 12, '用于缓解轻至中度疼痛', '通化茂祥', '一日三次', '孕妇禁用', 50, 10, 40, 0);
INSERT INTO drug (id, img, inPrice, outPrice, drugName, drugType, description, quality, detailDescription, factory, instruction, note, totalNumber, sentNumber, resetNumber, status) VALUES (5, null, 20, 30, '奥硝胶囊', '西药', '牙痛药', 12, '用于缓解轻至中度疼痛', '通化茂祥', '一日两次', '孕妇禁用', 50, 10, 40, 0);
INSERT INTO drug (id, img, inPrice, outPrice, drugName, drugType, description, quality, detailDescription, factory, instruction, note, totalNumber, sentNumber, resetNumber, status) VALUES (6, null, 10, 20, '云南白药', '非处方', '外敷药', 12, '用于缓解轻至中度疼痛', '通化茂祥', '一日两次', '孕妇禁用', 50, 10, 40, 0);
INSERT INTO drug (id, img, inPrice, outPrice, drugName, drugType, description, quality, detailDescription, factory, instruction, note, totalNumber, sentNumber, resetNumber, status) VALUES (7, null, 5, 10, '布洛芬', '非处方', '止痛', 12, '用于缓解轻至中度疼痛', '通化茂祥', '一日两次', '孕妇禁用', 50, 10, 40, 0);
INSERT INTO drug (id, img, inPrice, outPrice, drugName, drugType, description, quality, detailDescription, factory, instruction, note, totalNumber, sentNumber, resetNumber, status) VALUES (8, null, 15, 20, '止咳糖浆', '非处方', '止咳药', 12, '用于缓解轻至中度疼痛', '通化茂祥', '一日一次', '孕妇禁用', 50, 10, 40, 0);

-- 住院表
