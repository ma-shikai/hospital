/*
 Navicat Premium Data Transfer

 Source Server         : mysql8
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : hospital

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 22/08/2021 10:42:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for rolemenu
-- ----------------------------
DROP TABLE IF EXISTS `rolemenu`;
CREATE TABLE `rolemenu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `roleId` int(0) NOT NULL,
  `menuId` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `b`(`roleId`) USING BTREE,
  INDEX `c`(`menuId`) USING BTREE,
  CONSTRAINT `b` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `c` FOREIGN KEY (`menuId`) REFERENCES `menu` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_danish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rolemenu
-- ----------------------------
INSERT INTO `rolemenu` VALUES (100, 2, 4);
INSERT INTO `rolemenu` VALUES (101, 2, 5);
INSERT INTO `rolemenu` VALUES (102, 2, 12);
INSERT INTO `rolemenu` VALUES (103, 3, 6);
INSERT INTO `rolemenu` VALUES (104, 3, 12);
INSERT INTO `rolemenu` VALUES (105, 4, 1);
INSERT INTO `rolemenu` VALUES (106, 4, 12);
INSERT INTO `rolemenu` VALUES (107, 5, 2);
INSERT INTO `rolemenu` VALUES (108, 5, 3);
INSERT INTO `rolemenu` VALUES (109, 5, 12);
INSERT INTO `rolemenu` VALUES (116, 1, 7);
INSERT INTO `rolemenu` VALUES (117, 1, 8);
INSERT INTO `rolemenu` VALUES (118, 1, 9);
INSERT INTO `rolemenu` VALUES (119, 1, 10);
INSERT INTO `rolemenu` VALUES (120, 1, 11);
INSERT INTO `rolemenu` VALUES (121, 1, 12);

SET FOREIGN_KEY_CHECKS = 1;
