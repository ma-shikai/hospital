create table if not exists drughospital
(
    id int auto_increment
        primary key,
    drugId int not null,
    hospitalId int not null,
    need int null,
    own int null,
    constraint e
        foreign key (drugId) references drug (id),
    constraint f
        foreign key (hospitalId) references hospital (id)
)
    collate=utf8_danish_ci;

