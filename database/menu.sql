/*
 Navicat Premium Data Transfer

 Source Server         : mysql8
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : hospital

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 22/08/2021 10:43:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `menuName` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NULL DEFAULT NULL,
  `status` int(0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_danish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, '挂号信息管理', '/registration/index.html', 0);
INSERT INTO `menu` VALUES (2, '住院办理', '/hospital/index.html', 0);
INSERT INTO `menu` VALUES (3, '住院结算', '/hospital/account.html', 0);
INSERT INTO `menu` VALUES (4, '在院发药', '/hospital/dispensing.html', 0);
INSERT INTO `menu` VALUES (5, '药品管理', '/medicine/index.html', 0);
INSERT INTO `menu` VALUES (6, '检查收费项目登记', '/hospital/charge2.html', 0);
INSERT INTO `menu` VALUES (7, '收费项目管理', '/hospital/charge.html', 0);
INSERT INTO `menu` VALUES (8, '门诊医生管理', '/doctor/index.html', 0);
INSERT INTO `menu` VALUES (9, '用户管理', '/User/index.html', 0);
INSERT INTO `menu` VALUES (10, '角色管理', '/Role/index.html', 0);
INSERT INTO `menu` VALUES (11, '资源管理', '/Resource/index.html', 0);
INSERT INTO `menu` VALUES (12, '密码管理', '/User/password.html', 0);

SET FOREIGN_KEY_CHECKS = 1;
