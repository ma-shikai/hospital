/*
 Navicat Premium Data Transfer

 Source Server         : mysql8
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : hospital

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 22/08/2021 10:43:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sysuser
-- ----------------------------
DROP TABLE IF EXISTS `sysuser`;
CREATE TABLE `sysuser`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `loginName` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `trueName` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NULL DEFAULT NULL,
  `status` int(0) NOT NULL DEFAULT 0,
  `roleId` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `aa`(`loginName`) USING BTREE,
  INDEX `a`(`roleId`) USING BTREE,
  CONSTRAINT `a` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_danish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysuser
-- ----------------------------
INSERT INTO `sysuser` VALUES (1, 'admin', 'admin', 'admin', '123@qq.com', 0, 1);
INSERT INTO `sysuser` VALUES (2, 'xixi', 'xixi', '嘻嘻', '123@qq.com', 0, 2);
INSERT INTO `sysuser` VALUES (3, 'ada', 'ada', '阿达', '35463253@qq.com', 0, 5);
INSERT INTO `sysuser` VALUES (4, 'heihei', 'heihei', '黑黑', '35463253@qq.com', 0, 3);
INSERT INTO `sysuser` VALUES (5, 'lele', 'lele', '乐乐', '35463253@qq.com', 0, 5);
INSERT INTO `sysuser` VALUES (6, 'jiangbei', 'jiangbei', '江北', '35463253@qq.com', 1, 5);
INSERT INTO `sysuser` VALUES (7, 'yueyue', 'yueyue', '岳岳', '35463253@qq.com', 1, 5);
INSERT INTO `sysuser` VALUES (8, 'haer', 'haer', '哈尔', '123@qq.com', 1, 2);

SET FOREIGN_KEY_CHECKS = 1;
